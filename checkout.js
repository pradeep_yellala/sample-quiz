const username = document.getElementById("username");
const submitButton = document.getElementById("submitBtn");
const finalScore = document.getElementById("finalScore");
const playAgain = document.getElementById("playAgain");
const mostRecentScore = localStorage.getItem("mostRecentScore");

const highestScores = JSON.parse(localStorage.getItem("highestScore")) || [];
finalScore.innerText = `YOUR SCORE : ${mostRecentScore}`;

username.addEventListener("keyup", () => {
  submitButton.disabled = !username.value;
});

submitButton.addEventListener("click", (event) => {
  event.preventDefault();

  const scores = {
    name: username.value,
    score: mostRecentScore,
  };
  highestScores.push(scores);
  console.log(highestScores);
  highestScores.sort((a, b) => b.score - a.score);

  localStorage.setItem("highScores", JSON.stringify(highestScores));
  window.location.assign("/");
});

playAgain.addEventListener("click", () => {
  return window.location.assign("/quiz.html");
});
