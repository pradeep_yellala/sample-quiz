const question = document.getElementById("question");
const choices = Array.from(document.getElementsByClassName("choice btn"));
const nextButton = document.getElementById("next-button");
const questionCount = document.getElementById("questionCounter");
const scoreCounter = document.getElementById("scoreCounter");

let currentQuestion = {};
let acceptingAnswers = false;
let score = 0;
let questionCounter = 0;
let availableQuestions = [];

let questions = [
  {
    question: "What does GHz stand for?",
    choice1: "Gigahotz",
    choice2: "Gigahetz",
    choice3: "Gigahertz",
    choice4: "Gigahatz",
    answer: 3,
  },
  {
    question: "HTML is what type of language?",
    choice1: "Markup Language",
    choice2: "Scripting Language",
    choice3: "Programming Language",
    choice4: "Macro Language",
    answer: 1,
  },
  {
    question: "What amount of bits commonly equals one byte?",
    choice1: "1",
    choice2: "8",
    choice3: "64",
    choice4: "128",
    answer: 2,
  },
  {
    question:
      "The series of the Intel HD graphics generation succeeding that of the 5000 and 6000 series (Broadwell) is called:",
    choice1: "HD Graphics 700 ",
    choice2: "HD Graphics 600 ",
    choice3: "HD Graphics 500 ",
    choice4: "HD Graphics 400 ",
    answer: 3,
  },
  {
    question:
      "What is the code name for the mobile operating system Android 7.0?",
    choice1: "Ice Cream Sandwich",
    choice2: "Jelly Bean",
    choice3: "Marshmallow",
    choice4: "Nougat",
    answer: 4,
  },
  {
    question: "How many kilobytes in one gigabyte (in decimal)?",
    choice1: "1000000",
    choice2: "1024",
    choice3: "1000",
    choice4: "10000",
    answer: 1,
  },
  {
    question: "This mobile OS held the largest market share in 2012.",
    choice1: "Android",
    choice2: "BlackBerry",
    choice3: "Symbian",
    choice4: "IOS",
    answer: 4,
  },
  {
    question:
      "The C programming language was created by this American computer scientist. ",
    choice1: "Willis Ware",
    choice2: "Dennis Ritchie",
    choice3: "al-Khwārizmī",
    choice4: "Tim Berners Lee",
    answer: 2,
  },
  {
    question:
      "Which programming language shares its name with an island in Indonesia?",
    choice1: "C",
    choice2: "Python",
    choice3: "Jakarta",
    choice4: "Java",
    answer: 4,
  },
  {
    question: "How long is an IPv6 address?",
    choice1: "32 bits",
    choice2: "64 bits",
    choice3: "128 bits",
    choice4: "128 bytes",
    answer: 3,
  },
];

const marks = 10;
const maxQuestions = questions.length;

startQuiz = () => {
  questionCounter = 0;
  score = 0;
  availableQuestions = [...questions];
  getNewQuestion();
};

getNewQuestion = () => {
  questionCounter++;
  console.log(questionCounter);
  questionCount.innerText = `${questionCounter}/${maxQuestions}`;
  const questionNumber = Math.floor(Math.random() * availableQuestions.length);
  if (availableQuestions.length === 0) {
    localStorage.setItem("mostRecentScore", score);
    alert("the Quiz is over!! Check your score");
    return window.location.assign("/checkout.html");
  }
  currentQuestion = availableQuestions[questionNumber];
  question.innerText = currentQuestion.question;

  choices.forEach((choice) => {
    const number = choice.dataset["number"];
    choice.innerText = currentQuestion[`choice${number}`];
  });

  availableQuestions.splice(questionNumber, 1);
  acceptingAnswers = true;
};

choices.forEach((choice) => {
  choice.addEventListener("click", (event) => {
    if (!acceptingAnswers) return;

    acceptingAnswers = false;
    const selectedChoice = event.target;
    const selectedAnswer = selectedChoice.dataset["number"];

    if (selectedAnswer == currentQuestion.answer) {
      selectedChoice.style.backgroundColor = "green";
      scoreIncrement(marks);
      setTimeout(() => {
        selectedChoice.style.backgroundColor = "cornflowerblue";
      }, 1000);
    } else {
      selectedChoice.style.backgroundColor = "red";
      setTimeout(() => {
        selectedChoice.style.backgroundColor = "cornflowerblue";
      }, 1000);
    }
  });
});

nextButton.addEventListener("click", () => {
  getNewQuestion();
});

function scoreIncrement(num) {
  score += num;
  scoreCounter.innerText = score;
}
startQuiz();
