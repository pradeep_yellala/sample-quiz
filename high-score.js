const scores = document.getElementById("highestScoresList");

const highestScores = JSON.parse(localStorage.getItem("highScores"));

console.log(highestScores);

const highestScoresList = highestScores
  .map((score) => {
    return `<li>${score.name} - ${score.score}</li>`;
  })
  .join("");

scores.innerHTML = highestScoresList;
